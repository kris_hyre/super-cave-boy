{
    "id": "11967c08-518b-4149-9425-b0da4fa5222f",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fn_credits",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "97298474-4366-476c-a67b-c9f7a65d9f0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 94
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "19ee2380-f588-471e-8e8e-f299456fef2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 45,
                "y": 94
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4d654889-e532-4a94-94ca-fd16fb0b3850",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 229,
                "y": 71
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "1c296ed5-087f-4af4-97d2-4b15cb492f03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 195,
                "y": 25
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "591192cb-e88a-4987-9e70-b6da435a430d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 48
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "6117429d-eb6f-4992-af52-65d2b397fdac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e1c433a1-d32d-4fc9-bc30-98d90856414d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9125574c-06f4-4657-9369-168d306b2822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 35,
                "y": 94
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2285359e-51a6-4166-bdbc-12d80631e835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3e1677a5-5e32-411a-89a9-1b02dd1a624d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 94
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "591736a4-6662-44ed-965d-0375101d14c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 148,
                "y": 71
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9fa4bbdb-3dce-432e-a6a4-cf42e780dc95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 134,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2f4f7475-7994-43d3-8557-8139e030d677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 40,
                "y": 94
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ba115d6d-aab6-4042-be3c-cb3e461148df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 157,
                "y": 71
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7e4f5162-70a8-4ccb-844d-fa525f8e78c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 50,
                "y": 94
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "8111bfc6-e92c-4e13-887c-27fcfd8fb35b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 173,
                "y": 71
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3b4ea823-8cbb-43a1-b401-ed3c917763b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 110,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "812b0dcb-531a-4ed7-8326-9dd2f169f255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 213,
                "y": 71
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "364b4576-f9e9-465a-9053-4fa73529c84b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 122,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4807be64-28b1-4e1c-b3f4-2695e6aab5a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 146,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d0ab64fc-7e8e-4935-a225-93baa909286b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 71
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a945cdfa-582b-43d8-ac48-ab49e153c797",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 242,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "46f66193-6a45-4000-adbc-afc3f2f23cbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 230,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f14a5ab8-513c-4d02-af5a-2f92540002b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 218,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a1141560-b6aa-4c49-908c-91f68b5db89d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 206,
                "y": 48
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "006235f0-6125-4926-9cf8-7cdf28cfabcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 194,
                "y": 48
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c0e7666a-707e-41c8-ad2c-01f30ac5974a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 60,
                "y": 94
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f1146ee8-f40f-4275-b451-dbb31cdcccf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 65,
                "y": 94
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "bbaf1568-1de5-4500-bf86-bac0964c1283",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 48
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "423d4e48-6650-4547-b17b-8524cc57b49e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 208,
                "y": 25
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3f64b94d-4fba-4d22-8f6a-aa7d85e4f76a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 158,
                "y": 48
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d39fed87-3c61-4688-ac52-6ad863c8fe05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 170,
                "y": 48
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d5c67125-b1e2-4eee-a569-484a193dbff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0f00f4e4-5cc1-4520-a3f0-062acd448817",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "96a70e12-5bac-4343-ac7b-1fa69d910577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 156,
                "y": 25
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "28b5d868-e2d7-4803-9337-1135d03b0b0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "007b8890-76fe-4b29-9f78-39851c38302d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 17,
                "y": 25
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "0f7c707d-d612-40ca-abde-f4b3b698b4d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 143,
                "y": 25
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5286da63-d6de-4aef-8ef9-d903ea27a706",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e50c4c51-bd27-40ba-94a2-1721e83d0615",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "444f1794-0495-4e37-88be-abf9b0e3bce6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 31,
                "y": 25
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "78d53ad0-b7b1-439f-a0bf-eccfdbd1abe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 55,
                "y": 94
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6398c6a2-f4e8-4988-8832-713dc94b6cf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 82,
                "y": 71
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "31b62ab4-17ab-44e0-ab55-69c44eb5c13e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 45,
                "y": 25
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0b640332-8ca8-49d9-b796-8dbb7d9bf8be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 104,
                "y": 71
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4cab8c2a-1ef0-47a2-a52e-128ed8f47f58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "916965d6-fcd4-439f-a0a6-a8cfdd55519c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 59,
                "y": 25
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "536b953c-9fb2-49bc-bb4d-2767fdf717c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "bdda76f1-f4c2-4262-a095-d06c81704edb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 169,
                "y": 25
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "480b08da-f2ab-4e85-ac37-6f2793bce8fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "976f0d27-cafc-4c33-8232-68c9001133ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d0a0d379-4255-4c6b-9e5e-322e172e2bf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 129,
                "y": 25
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1436f4b3-9f67-4541-a777-a6034f01778e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 87,
                "y": 25
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "602faf53-4cd8-4fd1-a1ef-bd7758313e64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 101,
                "y": 25
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4ad1e084-4b44-4f26-9c80-785eca592830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f6bd80d7-112b-4ff7-8771-607331cd76a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ca3e0a41-9a76-423a-b7d3-984fcbd5a34a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f2f80f61-32cb-4a10-83b9-d85f394910a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4456a26d-23d9-4093-9e1b-9afed9487b5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 115,
                "y": 25
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e690b2b0-bb89-401d-87e6-6d1479b14b4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 29,
                "y": 94
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a355232d-7862-4c15-b039-92f4d3fa3799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 165,
                "y": 71
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "92bbe828-816c-4171-808d-551daa0ad765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 237,
                "y": 71
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f5fc4c67-3484-4469-a064-bfd8ab5cb9ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 137,
                "y": 71
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "273ad7d8-466a-43de-9f4d-01a4010eeb5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 73,
                "y": 25
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d50011ef-e49b-4b4a-a8ef-33b182263da8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 244,
                "y": 71
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "e2a3dde9-be04-4c23-9565-3e7bab1d0e9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 48
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "af24b91b-fb71-4ae2-889d-78b5b941a8b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 93,
                "y": 71
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "fe935808-5204-4abf-ac3b-b1c65d125a69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 48
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2712baf2-db51-44ed-8bc5-599f835fa53e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 48
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "69d2d36a-477b-4c79-b1e4-89c4912c8b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4ea50c9c-bc30-444b-9c93-c9aedd2aae2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 197,
                "y": 71
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f52669e8-5e57-4cbe-b9a9-3e47f2557903",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 232,
                "y": 25
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "38eef4c5-81a0-4003-bde6-11f86a37e53c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 71,
                "y": 71
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b494ec5d-97ce-407c-be89-f696107ad03a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 75,
                "y": 94
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ee04b095-14db-4ce5-a3a6-1e5a1feb1806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 23,
                "y": 94
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ef47aec0-71e8-43a5-a874-9d588ca83522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 49,
                "y": 71
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "317895e8-8b68-44fe-9ce4-94613b224dde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 79,
                "y": 94
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ae9194c6-f581-4eb9-bc73-5750d784b8b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6fc6f5f1-1939-4bfb-8e7d-cfc939434e07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 115,
                "y": 71
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "320c2869-a914-407b-b58f-3897ec32e635",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 220,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c6b80571-aa55-42a9-89ab-e5852837a64c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 60,
                "y": 71
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8899580f-d877-4856-9bb8-78b5ee044bf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 48
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1e87c08a-1568-4d28-9dce-15a1c9c8d45c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 181,
                "y": 71
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "26ae7622-e4b7-4ee4-817e-38d77402510c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 126,
                "y": 71
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "633f8d74-c3dc-4ab4-940a-b05551ad96ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 221,
                "y": 71
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0f502185-aad1-469d-bf23-8a03caed31bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 38,
                "y": 71
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b8a08b1e-bb75-4e3a-a24d-d2fef3c10b63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 86,
                "y": 48
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "92b46f6f-e43e-4965-afe4-6222083cff67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d964760d-b0a7-4d61-b394-7f468fbcf1ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 98,
                "y": 48
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d8af43f9-f6b1-45a6-93e8-aa9bbb620dbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 71
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9444b4c4-617f-4375-a6ae-85c87f392291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 182,
                "y": 48
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b94a43f1-4295-4771-9f22-8b2429ce4027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 189,
                "y": 71
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3fc1d89a-8f14-46a3-9cef-8992be7ea23d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 70,
                "y": 94
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "649be84f-76aa-4b9c-bd1f-fa77d9de6743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 205,
                "y": 71
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3942863c-c86a-4cc4-83bf-22d87fa91451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 182,
                "y": 25
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "63e1db9c-467a-476c-9392-11c0f970fc70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "c3227126-6823-4c0c-bef1-1565b3a83bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "67eaf896-df17-45d6-b026-8a7b2ac1ae47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "fa9890fe-c663-4e2f-8417-7c80db304cdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "b1e747d2-7f08-4e55-aec4-9a8a6e4352b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "3a26986d-bec5-497d-a545-2ecc6d3cdeaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "698a2f88-9143-4c0f-9bfc-47d6f70a2e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "a2b2a778-6d1c-497f-bfa8-cc93306d1c04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "2ddd8682-85fa-46e4-8339-39347a20bd97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "569147cb-60eb-4444-b5ea-72e13ac2b2a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "18f0b98f-c7ca-46a6-b0da-f0d79ae3f1a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "63e1e215-a4aa-4390-a1d1-1db2104494e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "034957a4-24de-409a-bf11-1034b80608cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "140b1935-3e50-4333-b979-2944a2bc95ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "4a1de5c3-8957-4952-957e-58d45a2b8c41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "c0b71af9-f528-4035-a546-3bd3730641c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "15879e8d-4a7a-49e9-b70a-3a09ef1c8b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "e314d338-bc61-44fd-a1a0-5232fceb580c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "2eafb667-d8a6-4bcb-84a3-8f112016c8ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "9e05abaa-7613-43c4-bbdf-aa4b75185d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "e842c55b-801c-4ed3-99cd-05a47b449f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "e17a5d70-060c-486f-afe1-6e12dd81a7a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "efe88d3e-72e1-4eb0-9ffd-ddb37ecd66cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "87be36dc-4563-458c-a9fe-6c5f35cdf76d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "51d27c4a-c69d-437d-b8ea-a54d0b487f50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "aed31282-b6b1-476d-b84f-e6ab81ef3e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "872b0c51-8d98-4584-a9f0-399140545a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "190510b7-b5b6-4ab0-ae49-613b1b433b06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "f8cb0867-6255-46df-a989-d24fd94af90d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "27af0fc5-fe79-415d-ac27-2d322afb5307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "27bf9627-8b9a-414b-a895-36ca68b208b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "77d2fe0e-54d3-41c4-9d8c-f8886a379864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "3b2a2e57-3264-4875-b988-71969f510598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "95a03938-eeea-41fc-a985-2e830759594b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "a2110716-b81f-4fb6-bb00-710e9d5fa435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "db16ee7a-7841-420c-a372-cf686dae9a12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "4c5a81f8-76cc-41b9-b02d-b48f5692dcc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "abe52a84-2ad6-4ebe-ab56-70fc3388a753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "ca4f6bb3-aacd-4213-8b80-6cb8f4ff1bb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "fb698f97-bfec-4c57-b23a-4c2f45124fde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "475d3169-bbcf-4dc5-bef9-6b6cb8518a59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "0ea318e8-fada-4af5-93f4-d02e9c433a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "0086557a-efd8-4425-a762-c624eae32d62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "3a0d6fbb-be58-47b5-89f8-75b89d895c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "ab47026e-7a9b-4691-a29c-b8b926273563",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "658276e3-37ab-4514-8986-c70073c49100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "7a39d7b2-7b43-4960-bd7f-54b38c3a2486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "85de4934-8e99-4a3e-8924-f5902177d5b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "bd75598d-ff48-4db3-97d9-4a6326d0e72c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "6a3d8a20-07f1-4c76-9011-006892262ff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "3bd5eb75-917e-47d5-b677-5374ddb53b51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "f004e6d1-1485-4407-b295-8c8b4bffc5f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "be46aff2-5561-4020-872e-98e3003841ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "43faf701-81b6-46e6-a9f1-9826a451a026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "c5358246-2e77-4e6a-9311-47077072f18c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "7dfe9632-3c76-44ce-9bf2-5d9202fd9111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "5ee9a688-a55e-4896-b4fe-622e2a665e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "c80b2c74-001b-4d44-9b5b-f35da1844d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "a29a7d1f-0619-4bae-a62b-b9e1c3cec318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "ac7403cd-401b-499d-85a7-cd3524fba4d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "453ecb32-163e-44fd-bd75-a5f91e4bef01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "643d8a91-4b38-49ca-989c-2118706e8e7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "176fa719-88fc-4747-b090-797cf89f0560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "2fc4d9f2-5169-4432-b0ce-534077974fd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "40793604-0cdd-40d0-8a0d-dbacc56efaca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "55ad0877-2f76-44cc-bbf1-417533b94aeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "b0e4b69f-53b1-4019-966c-0acb4e7826b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "cd87a95f-6262-474e-b734-5ffc57fed63d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "0b709fea-0572-467e-a9b0-7e988d1015c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "6e4428ba-f894-435b-b7e4-48e4d84c8729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "84af090f-22bd-4e8d-84ce-a06821530be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "eff34dab-31d8-4263-bba0-2eb94dda3cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "381a4e46-dc37-479b-a6d3-cb9adf1278f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "50380ffb-387b-4b44-8507-6bb17409331d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "034a2287-33fc-43ae-ab4f-9ce613bd7f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "a64bd48c-5d1d-4d61-8c25-461fa6c5f55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "60fb3a72-6b7c-4d20-94c2-46ba8f454470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "b4b25c3e-4655-4878-9894-66eda2888efe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "ad42a68e-1802-4f72-a25b-30731b84b9d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "4e6cb57e-6797-4727-984c-0e506d66a4c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "1a26933b-d237-4433-a757-aa81612095f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "4775a822-87d2-42cc-aad8-909a81d398f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "608f72e8-1cf5-4dde-bd9a-d99e7331e753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "3db01608-9f11-40a3-972e-dbe4feb21381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "680a1c32-7505-4abc-95b2-0d26e5fff7f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "2154b3e3-3a86-488b-abbc-a6488ccf14e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "584085e4-6f67-48b5-a16a-f334f1409843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "c093e6ff-ad29-482d-9a66-0613408abb2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}