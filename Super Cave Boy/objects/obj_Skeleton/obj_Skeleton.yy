{
    "id": "4bb10dd0-191a-4244-b7e8-ca6729d110c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Skeleton",
    "eventList": [
        {
            "id": "14ec5f77-8be7-4b4a-ba22-07ecd471edaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4bb10dd0-191a-4244-b7e8-ca6729d110c3"
        },
        {
            "id": "4ecc6274-b9df-4f7c-8740-5183dade3643",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4bb10dd0-191a-4244-b7e8-ca6729d110c3"
        },
        {
            "id": "3ffa0225-6ee6-40cb-b2a2-88795938cfd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "4bb10dd0-191a-4244-b7e8-ca6729d110c3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2bcbb83b-f903-4e62-99ab-e525c692cf0b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75a520f9-bfb2-4b01-8460-122edab1e0a8",
    "visible": true
}