/// Draw player GUI

var start_text;
var exit_text;
var 

if (gamepad_is_connected(0)){ 
	exit_text = "Press [Back] or [Esc] to Exit";
	start_text = "Press [Start] or [Space] to Play \n \n Press [c] for Credits \n \n" + exit_text;
	
} else {
	exit_text = "Press [Esc] to Exit";
	start_text = "Press [Space] to Play \n \n Press [c] for Credits" + exit_text;
	
}

var credits_text = @"Music:
Clenched Teeth by Kevin MacLeod (incompetech.com) 
Licensed under Creative Commons: By Attribution 3.0
http://creativecommons.org/licenses/by/3.0/

Malicious by Kevin MacLeod (incompetech.com) 
Licensed under Creative Commons: By Attribution 3.0
http://creativecommons.org/licenses/by/3.0/

Art and Sound Effects:
by Benjamin Kean Anderson, copyright 2015

Programming:
by Kris Hyre, based off the course
Make a Platform Game! by Benjamin Kean Anderson
https://learn.heartbeast.co/

Press [B] to go back to the Start Menu
";



		
switch (room){
	case rm_menu:
		draw_set_halign(fa_center);
		draw_set_font(fn_title);
		draw_text_color(room_width + 2, room_height / 2 + 3, "Super Cave Boy", c_black, c_black, c_black, c_black, 1);
		draw_text_color(room_width, room_height / 2, "Super Cave Boy", c_white, c_white, c_white, c_white, 1);
				
		draw_set_font(fn_start);
		draw_text_color(room_width + 2, room_height + 3, start_text, c_black, c_black, c_black, c_black, 1);
		draw_text_color(room_width, room_height, start_text, c_white, c_white, c_white, c_white, 1);		
		break;
		
	case rm_high_score:
		draw_set_halign(fa_center);
		draw_set_font(fn_start);
		var current_score_string = "Your score was " + string(score);
		var high_score_string = "The high score is " + string(obj_Player_Stats.high_score);
		draw_text_color(room_width, room_height + 3, current_score_string, c_white, c_white, c_white, c_white, 1);
		draw_text_color(room_width, room_height + 43, high_score_string, c_white, c_white, c_white, c_white, 1);
		draw_text_color(room_width, room_height + 83, exit_text, c_white, c_white, c_white, c_white, 1);
		break;
		
	case rm_credits:
		draw_set_halign(fa_center);
		draw_set_font(fn_credits);
		draw_text_color(room_width, room_height - 43, credits_text, c_white, c_white, c_white, c_white, 1);
		break;
		
	default:
		// Draw empty hearts
		for (var i = 0; i < max_hp; i++) {
			draw_sprite_ext(spr_heart, 0, 24 + i * 36, 20, 1, 1, 0, c_black, 0.5);
		}
		
		// Draw active hearts
		for (var i = 0; i < hp; i++) {
			draw_sprite_ext(spr_heart, 0, 24 + i * 36, 20, 1, 1, 0, c_white, 1);
		}
		
		// Draw sapphire count
		var saph_string = "x" + string(sapphires);
		var text_width = string_width(saph_string);
		draw_set_halign(fa_right);
		draw_set_font(fn_start);
		draw_text_color(view_wview[0] * 2 - 16, 22, saph_string, c_white, c_white, c_white, c_white, 1);
		draw_sprite(spr_gui_sapphire, 0, view_wview[0] * 2 - 32 - text_width, 22);
		
		// Track time
		time += 1;
		
		// Draw timer
		draw_text_color(view_wview[0] - 16, 22, "Time: " + string(floor(time / room_speed)), c_white, c_white, c_white, c_white, 1);
		
		break;
}


