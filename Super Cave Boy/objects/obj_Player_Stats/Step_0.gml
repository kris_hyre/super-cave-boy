// Starting the game
var start;

if (gamepad_is_connected(0)){
	start = (gamepad_button_check_pressed(0, gp_start) || keyboard_check_pressed(vk_space));
} else {
	start = keyboard_check_pressed(vk_space);	
}

if (room == rm_menu and start){
	room_goto(rm_0);	
}

if (room == rm_menu and keyboard_check_pressed(ord("C"))){
	room_goto(rm_credits);
}

if (room == rm_credits and keyboard_check_pressed(ord("B"))){
	room_goto(rm_menu);
}

if (gamepad_button_check_pressed(0, gp_select) or keyboard_check_pressed(vk_escape)){
	game_end();	
}