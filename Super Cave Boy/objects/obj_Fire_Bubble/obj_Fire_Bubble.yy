{
    "id": "4f64c114-bcc6-4420-9fae-2c0f11c87f91",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Fire_Bubble",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c48c2d65-f035-4367-a339-b123a2872e82",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a05dbb27-17c8-478f-b481-fdec851340a7",
    "visible": true
}