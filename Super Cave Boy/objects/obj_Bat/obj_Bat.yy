{
    "id": "108853d9-b03a-40ec-9fbc-290601e08a0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Bat",
    "eventList": [
        {
            "id": "92c5f4ed-1201-4c52-ba2f-979e65c795c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "108853d9-b03a-40ec-9fbc-290601e08a0c"
        },
        {
            "id": "d35742b7-b65f-4815-ae44-08d0d11713e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "108853d9-b03a-40ec-9fbc-290601e08a0c"
        },
        {
            "id": "1672038a-bd4c-4458-ac41-bc790c417fc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "108853d9-b03a-40ec-9fbc-290601e08a0c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2bcbb83b-f903-4e62-99ab-e525c692cf0b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7a6b0fd5-48e2-455b-9a37-6babfd14c443",
    "visible": true
}