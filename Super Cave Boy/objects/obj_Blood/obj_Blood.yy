{
    "id": "c48c2d65-f035-4367-a339-b123a2872e82",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Blood",
    "eventList": [
        {
            "id": "53450b7e-7dc5-43b5-9a24-35989f46a701",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c48c2d65-f035-4367-a339-b123a2872e82"
        },
        {
            "id": "9c609513-6e40-4009-80d2-2730f142b75f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "530bc875-eea7-46b3-acb9-9df5f4cbe7ad",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c48c2d65-f035-4367-a339-b123a2872e82"
        },
        {
            "id": "1bcb3e05-07b0-43a4-8838-9b95977bc827",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "c48c2d65-f035-4367-a339-b123a2872e82"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ae395802-8ebb-4baa-ae93-80312c94a0c9",
    "visible": true
}