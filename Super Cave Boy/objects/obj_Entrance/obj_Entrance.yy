{
    "id": "02dabf7f-a441-4cca-970f-8ffa53d75626",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Entrance",
    "eventList": [
        {
            "id": "4cef7a0f-54d3-4347-968e-5afee26d409c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "02dabf7f-a441-4cca-970f-8ffa53d75626"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1372c12-b0f1-4b29-97ba-2369d9fd16af",
    "visible": true
}