{
    "id": "2bcbb83b-f903-4e62-99ab-e525c692cf0b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Enemy_Parent",
    "eventList": [
        {
            "id": "a5479fbd-7f70-472d-8fc7-85b4fcfe9b5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "2bcbb83b-f903-4e62-99ab-e525c692cf0b"
        },
        {
            "id": "f1c1330f-44ff-4e90-9d60-c8da7e98df4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2bcbb83b-f903-4e62-99ab-e525c692cf0b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "68ab00ea-a4d9-4702-9375-74d4e5d2c052",
    "visible": true
}