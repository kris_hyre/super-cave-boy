{
    "id": "da999c47-f909-4d60-a42e-77508a0837cc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Dirt",
    "eventList": [
        {
            "id": "19482e9e-b61e-4b0a-8c2f-1a77ae320ada",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "da999c47-f909-4d60-a42e-77508a0837cc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "530bc875-eea7-46b3-acb9-9df5f4cbe7ad",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "bbb23f4a-f1b5-49ca-a30c-64382d9972f9",
    "visible": true
}