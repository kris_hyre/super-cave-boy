{
    "id": "1f8011a5-b1cb-4791-9d3d-9a869fc313e8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Snake",
    "eventList": [
        {
            "id": "9a7248e5-1cbd-42d0-b9e6-728bd8cc8f21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f8011a5-b1cb-4791-9d3d-9a869fc313e8"
        },
        {
            "id": "2d82051c-74f7-42af-b1b8-4f703f4f26e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1f8011a5-b1cb-4791-9d3d-9a869fc313e8"
        },
        {
            "id": "50b371ff-c4b4-40b2-a4f3-4353ce6c5dfe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1f8011a5-b1cb-4791-9d3d-9a869fc313e8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2bcbb83b-f903-4e62-99ab-e525c692cf0b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "474bd0fb-7790-4379-9041-84336500b8d4",
    "visible": true
}