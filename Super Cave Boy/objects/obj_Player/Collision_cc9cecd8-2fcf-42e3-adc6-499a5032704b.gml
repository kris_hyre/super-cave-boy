/// Collide with an Enemy

var above_enemy = y < other.y + vspd;
var falling = vspd > 0;

if (above_enemy && (falling || state = ledge_grab_state)) {
	if (!place_meeting(x, yprevious, obj_Solid)) {
		y = yprevious;
	}
	
	while (!place_meeting(x, y + 1, other)) {
		y++;
	}

	with (other) {
		instance_destroy();
	}	
	
	// Bounce off
	vspd = -(jspd / 1.5);
	
	audio_play_sound(snd_step, 6, false);
	
} else {
	take_damage();
}