{
    "id": "cbe2ca78-686a-4273-887f-52a45b6717c8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Player",
    "eventList": [
        {
            "id": "749e4aa9-fb5c-442a-a42a-28ac9ef8d51e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cbe2ca78-686a-4273-887f-52a45b6717c8"
        },
        {
            "id": "e7a0f26b-bd38-49d8-b5dc-4e5c0ae9d446",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cbe2ca78-686a-4273-887f-52a45b6717c8"
        },
        {
            "id": "cc9cecd8-2fcf-42e3-adc6-499a5032704b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2bcbb83b-f903-4e62-99ab-e525c692cf0b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cbe2ca78-686a-4273-887f-52a45b6717c8"
        },
        {
            "id": "43573363-115d-45a4-b4cb-dc0003b7f13b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e0631230-fc78-44f3-97c8-651e63793db6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cbe2ca78-686a-4273-887f-52a45b6717c8"
        },
        {
            "id": "e60e6531-377b-4864-9f29-cceea7612b02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e0f20feb-5240-45e6-baac-0fbcc3e04f6e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cbe2ca78-686a-4273-887f-52a45b6717c8"
        },
        {
            "id": "5c1d4794-29ae-4e33-9c99-d55dcc3a0f4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "94b7b9ae-9328-4458-88c7-0aa20dc50b14",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cbe2ca78-686a-4273-887f-52a45b6717c8"
        },
        {
            "id": "d30ae9ba-b73d-4399-8354-73f9d5ece6d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "73b045ae-fae5-4f98-a87f-d29b6d450881",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cbe2ca78-686a-4273-887f-52a45b6717c8"
        },
        {
            "id": "f99569a4-a44d-4374-9620-f99771073f43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cbe2ca78-686a-4273-887f-52a45b6717c8"
        }
    ],
    "maskSpriteId": "7faef636-db3a-48c2-9613-1d05c01b8a55",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f32d06f1-a15c-48dd-a0b3-5fa627759af4",
    "visible": true
}