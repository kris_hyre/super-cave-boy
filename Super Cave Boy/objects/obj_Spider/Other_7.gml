///Jump

if (state = spider_idle_state) {
	if (instance_exists(obj_Player)) {
		hspd = sign(obj_Player.x -x) * spd
		vspd = -abs(hspd * 2);
	}
	horizontal_move_bounce(obj_Solid);
	state = spider_jump_state;
}