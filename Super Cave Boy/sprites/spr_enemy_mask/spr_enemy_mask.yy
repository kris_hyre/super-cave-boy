{
    "id": "68ab00ea-a4d9-4702-9375-74d4e5d2c052",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "97aca6c4-7dee-4060-830e-9c69cc746fd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ab00ea-a4d9-4702-9375-74d4e5d2c052",
            "compositeImage": {
                "id": "88420f00-f31f-44f0-b5a7-a299060857f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97aca6c4-7dee-4060-830e-9c69cc746fd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25d93286-aaad-4252-88f9-6f52726a9520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97aca6c4-7dee-4060-830e-9c69cc746fd4",
                    "LayerId": "ca70a5e7-442f-48d8-ba57-f6253605a4b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "ca70a5e7-442f-48d8-ba57-f6253605a4b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68ab00ea-a4d9-4702-9375-74d4e5d2c052",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}