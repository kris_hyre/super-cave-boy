{
    "id": "c3e58e93-ad51-4c7f-a409-c906318e0b9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lava",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa29f567-f3ff-4021-b387-1cf9acbd0cd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3e58e93-ad51-4c7f-a409-c906318e0b9e",
            "compositeImage": {
                "id": "9f5b4968-cf43-4254-bd66-128e7a34ff75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa29f567-f3ff-4021-b387-1cf9acbd0cd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3129b20-90d4-4ab3-8eb4-8624b46f1236",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa29f567-f3ff-4021-b387-1cf9acbd0cd6",
                    "LayerId": "9c305d01-8487-4ea1-a85d-a6bbc4472ff1"
                }
            ]
        },
        {
            "id": "0222dcc4-a442-4244-a3c5-65cdb32e5f84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3e58e93-ad51-4c7f-a409-c906318e0b9e",
            "compositeImage": {
                "id": "b0a2504d-fd74-4dc8-b201-a17d39d047c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0222dcc4-a442-4244-a3c5-65cdb32e5f84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4c70b73-17b0-4ee5-9f6c-dde12ad70f66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0222dcc4-a442-4244-a3c5-65cdb32e5f84",
                    "LayerId": "9c305d01-8487-4ea1-a85d-a6bbc4472ff1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "9c305d01-8487-4ea1-a85d-a6bbc4472ff1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3e58e93-ad51-4c7f-a409-c906318e0b9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}