{
    "id": "fe6ab526-0650-4963-945d-d9fc7845807f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeleton_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 2,
    "bbox_right": 41,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58fd9681-88c2-4079-82b1-ea6f894ecc84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe6ab526-0650-4963-945d-d9fc7845807f",
            "compositeImage": {
                "id": "0f227c8f-5fdc-40ea-9e66-c05d13a2e6c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58fd9681-88c2-4079-82b1-ea6f894ecc84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55bdc132-be7a-49dc-b738-ebf2ea291b3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58fd9681-88c2-4079-82b1-ea6f894ecc84",
                    "LayerId": "2823956b-0efa-466c-87c3-4d4a0aa2e27c"
                }
            ]
        },
        {
            "id": "4d049342-af26-484e-9070-9b9e03c4de71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe6ab526-0650-4963-945d-d9fc7845807f",
            "compositeImage": {
                "id": "573756c3-5cbe-4fbf-9e6a-d2ba1abc9879",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d049342-af26-484e-9070-9b9e03c4de71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0823be9-bc6b-4ad6-92e7-b730b6453baa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d049342-af26-484e-9070-9b9e03c4de71",
                    "LayerId": "2823956b-0efa-466c-87c3-4d4a0aa2e27c"
                }
            ]
        },
        {
            "id": "1fc16ef2-eadc-45fd-9bf8-4c97accb52f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe6ab526-0650-4963-945d-d9fc7845807f",
            "compositeImage": {
                "id": "9aceebeb-06c1-4752-a308-b8f135c35d71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fc16ef2-eadc-45fd-9bf8-4c97accb52f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff96dcac-afa8-4967-b3bc-6a063fe148ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fc16ef2-eadc-45fd-9bf8-4c97accb52f2",
                    "LayerId": "2823956b-0efa-466c-87c3-4d4a0aa2e27c"
                }
            ]
        },
        {
            "id": "90147a71-29b6-420f-826a-6f2b46993166",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe6ab526-0650-4963-945d-d9fc7845807f",
            "compositeImage": {
                "id": "bfdc0969-e007-437a-ae8d-291a902ccaca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90147a71-29b6-420f-826a-6f2b46993166",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62e1104b-fe27-41bc-9be2-56fdf56e7b7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90147a71-29b6-420f-826a-6f2b46993166",
                    "LayerId": "2823956b-0efa-466c-87c3-4d4a0aa2e27c"
                }
            ]
        },
        {
            "id": "dfec874b-34c8-4e9a-ab5a-343a410ef7e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe6ab526-0650-4963-945d-d9fc7845807f",
            "compositeImage": {
                "id": "906bbe7f-be59-44ae-861d-5333c30a0841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfec874b-34c8-4e9a-ab5a-343a410ef7e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b82e5766-2533-4394-a77b-683fed0288cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfec874b-34c8-4e9a-ab5a-343a410ef7e8",
                    "LayerId": "2823956b-0efa-466c-87c3-4d4a0aa2e27c"
                }
            ]
        },
        {
            "id": "ba789639-14ef-4225-8bcb-554d7fcb8927",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe6ab526-0650-4963-945d-d9fc7845807f",
            "compositeImage": {
                "id": "b749b185-10d8-4506-a1b3-20a9c78962f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba789639-14ef-4225-8bcb-554d7fcb8927",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6db1a0a0-4309-406c-a3ac-e227d4c6db1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba789639-14ef-4225-8bcb-554d7fcb8927",
                    "LayerId": "2823956b-0efa-466c-87c3-4d4a0aa2e27c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 63,
    "layers": [
        {
            "id": "2823956b-0efa-466c-87c3-4d4a0aa2e27c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe6ab526-0650-4963-945d-d9fc7845807f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 31
}