{
    "id": "4abe5583-26e7-4cd9-87e0-3e51bf5eaade",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "957adc54-6dfa-4abb-9b7c-0a9bd8f401d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4abe5583-26e7-4cd9-87e0-3e51bf5eaade",
            "compositeImage": {
                "id": "b7003243-0c00-4cb2-ae84-2c976afa52e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "957adc54-6dfa-4abb-9b7c-0a9bd8f401d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d40ceca-2bba-41c1-9a75-18e9b47cbece",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "957adc54-6dfa-4abb-9b7c-0a9bd8f401d8",
                    "LayerId": "dd023855-0962-4348-90f5-24e59317175a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dd023855-0962-4348-90f5-24e59317175a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4abe5583-26e7-4cd9-87e0-3e51bf5eaade",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}