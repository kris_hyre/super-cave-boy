{
    "id": "c9808762-3485-4ba6-b419-53e4d20bf539",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "12c80c81-10d6-48e3-bea8-188ff762c83e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9808762-3485-4ba6-b419-53e4d20bf539",
            "compositeImage": {
                "id": "3aede8cb-681e-4aaa-a377-dedd43c9dc4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12c80c81-10d6-48e3-bea8-188ff762c83e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "653ca8b9-4c19-44d5-a654-bdae6edfbd60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12c80c81-10d6-48e3-bea8-188ff762c83e",
                    "LayerId": "ae001df9-9fd9-41aa-bc49-b012761d7a6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "ae001df9-9fd9-41aa-bc49-b012761d7a6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9808762-3485-4ba6-b419-53e4d20bf539",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}