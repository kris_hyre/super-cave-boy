{
    "id": "c9033577-f9cf-4f8f-932f-eeadc9812333",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gui_sapphire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dd78a55-e6d6-4845-8583-33ccc6ca8f93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9033577-f9cf-4f8f-932f-eeadc9812333",
            "compositeImage": {
                "id": "e0676d6e-dcc5-4cf4-9e02-c6f00b6c6618",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dd78a55-e6d6-4845-8583-33ccc6ca8f93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ba15602-fd3b-4358-93f9-95d570e11284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dd78a55-e6d6-4845-8583-33ccc6ca8f93",
                    "LayerId": "c5f29285-27d7-4bbb-87ee-b1a05393b0e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c5f29285-27d7-4bbb-87ee-b1a05393b0e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9033577-f9cf-4f8f-932f-eeadc9812333",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 2,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 31,
    "yorig": 0
}