{
    "id": "44aa998f-9757-4718-95a1-ee391caf025e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 269,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49564d56-2519-4f9a-aa20-1f6b9929a7de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44aa998f-9757-4718-95a1-ee391caf025e",
            "compositeImage": {
                "id": "22a2ffa2-9331-4597-80d4-cccfde8abf18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49564d56-2519-4f9a-aa20-1f6b9929a7de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aa2104c-7d85-4b93-8d42-8f074b372841",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49564d56-2519-4f9a-aa20-1f6b9929a7de",
                    "LayerId": "5606b5a6-08b5-45fc-8c6e-1726f734ceb9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "5606b5a6-08b5-45fc-8c6e-1726f734ceb9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44aa998f-9757-4718-95a1-ee391caf025e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}