{
    "id": "e1372c12-b0f1-4b29-97ba-2369d9fd16af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_entrance",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "273162e0-56b5-4ad2-8a15-018eb8e55c80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1372c12-b0f1-4b29-97ba-2369d9fd16af",
            "compositeImage": {
                "id": "44a26a2a-85e7-4436-b766-35545afff491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "273162e0-56b5-4ad2-8a15-018eb8e55c80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f241bcb6-885a-4f60-99d4-aade4c1be6e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "273162e0-56b5-4ad2-8a15-018eb8e55c80",
                    "LayerId": "f5f5b10d-a728-49a5-aa64-35bceb6a6576"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "f5f5b10d-a728-49a5-aa64-35bceb6a6576",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1372c12-b0f1-4b29-97ba-2369d9fd16af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}