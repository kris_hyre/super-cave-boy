{
    "id": "f32d06f1-a15c-48dd-a0b3-5fa627759af4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "14b23501-de7f-46e8-801e-8c608533f519",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f32d06f1-a15c-48dd-a0b3-5fa627759af4",
            "compositeImage": {
                "id": "3a7e22a3-1598-48f8-b833-daf3f2e8a3f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14b23501-de7f-46e8-801e-8c608533f519",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2ccd811-cee2-4b2c-9216-61eaac509886",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14b23501-de7f-46e8-801e-8c608533f519",
                    "LayerId": "209e528c-b50b-4c43-a461-a9c6c1c3e6af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "209e528c-b50b-4c43-a461-a9c6c1c3e6af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f32d06f1-a15c-48dd-a0b3-5fa627759af4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}