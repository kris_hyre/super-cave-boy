{
    "id": "873f09f6-063f-42ee-9b13-90d7ea79e32a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_snake_jumping",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f7de47bc-429d-42ed-aeaa-ab8509039047",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "873f09f6-063f-42ee-9b13-90d7ea79e32a",
            "compositeImage": {
                "id": "d6157a1f-286d-4dd1-a295-aabc705b351d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7de47bc-429d-42ed-aeaa-ab8509039047",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd4b4e1c-10b6-4efa-8085-2b715c0f7efb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7de47bc-429d-42ed-aeaa-ab8509039047",
                    "LayerId": "a56d60a1-aea8-40af-87e8-636f47cd239c"
                }
            ]
        },
        {
            "id": "d19e6217-275f-47d2-ab24-21bf6ba33f74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "873f09f6-063f-42ee-9b13-90d7ea79e32a",
            "compositeImage": {
                "id": "8d1379ab-38cf-4a73-9e01-bfd7bde4e974",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d19e6217-275f-47d2-ab24-21bf6ba33f74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65b01192-3b76-4de4-84a1-cf96ccf869e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d19e6217-275f-47d2-ab24-21bf6ba33f74",
                    "LayerId": "a56d60a1-aea8-40af-87e8-636f47cd239c"
                }
            ]
        },
        {
            "id": "107cdeea-6ca5-4f6c-8725-f33c02a324fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "873f09f6-063f-42ee-9b13-90d7ea79e32a",
            "compositeImage": {
                "id": "ed1fc02b-e655-4da0-99e3-0e97e3ca31dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "107cdeea-6ca5-4f6c-8725-f33c02a324fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42af8ad1-0b4d-4205-9d08-3dd51547a169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "107cdeea-6ca5-4f6c-8725-f33c02a324fa",
                    "LayerId": "a56d60a1-aea8-40af-87e8-636f47cd239c"
                }
            ]
        },
        {
            "id": "3f61a0bf-842d-4608-9694-9856e04f9cf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "873f09f6-063f-42ee-9b13-90d7ea79e32a",
            "compositeImage": {
                "id": "9dae0e7e-c263-4ca1-ba13-54d6bce34a85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f61a0bf-842d-4608-9694-9856e04f9cf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db42f73c-3047-438e-a389-37e61e4de622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f61a0bf-842d-4608-9694-9856e04f9cf3",
                    "LayerId": "a56d60a1-aea8-40af-87e8-636f47cd239c"
                }
            ]
        },
        {
            "id": "8da39bab-639b-41a6-88b9-82fc6c5994d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "873f09f6-063f-42ee-9b13-90d7ea79e32a",
            "compositeImage": {
                "id": "6932cf00-2aa6-447e-b575-4fcc99c6bf3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8da39bab-639b-41a6-88b9-82fc6c5994d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0677916c-7fc5-4a15-9795-72afbcc226fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8da39bab-639b-41a6-88b9-82fc6c5994d2",
                    "LayerId": "a56d60a1-aea8-40af-87e8-636f47cd239c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a56d60a1-aea8-40af-87e8-636f47cd239c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "873f09f6-063f-42ee-9b13-90d7ea79e32a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}