{
    "id": "6f7f8448-6f91-4549-aa0a-10784a0cd821",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 12,
    "bbox_right": 67,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9d851f8-6379-4e1e-a919-62ef72191e67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f7f8448-6f91-4549-aa0a-10784a0cd821",
            "compositeImage": {
                "id": "e8337c40-4233-49ae-9033-604057db9d1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9d851f8-6379-4e1e-a919-62ef72191e67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48119d29-f5db-463f-b606-4cbac31439c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9d851f8-6379-4e1e-a919-62ef72191e67",
                    "LayerId": "124057f9-66a1-4ac2-84bf-39732b43e93c"
                }
            ]
        },
        {
            "id": "8d3647a0-850d-44cb-9760-1f63310c7374",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f7f8448-6f91-4549-aa0a-10784a0cd821",
            "compositeImage": {
                "id": "8f868526-ed0c-4802-93b6-c5d247146078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d3647a0-850d-44cb-9760-1f63310c7374",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47f2bf11-f2e3-4916-9676-1ef5643d3161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d3647a0-850d-44cb-9760-1f63310c7374",
                    "LayerId": "124057f9-66a1-4ac2-84bf-39732b43e93c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 94,
    "layers": [
        {
            "id": "124057f9-66a1-4ac2-84bf-39732b43e93c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f7f8448-6f91-4549-aa0a-10784a0cd821",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 94
}