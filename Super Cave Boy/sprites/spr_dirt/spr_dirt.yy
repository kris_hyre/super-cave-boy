{
    "id": "bbb23f4a-f1b5-49ca-a30c-64382d9972f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dirt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "35d5d87c-2968-43f4-ac7b-ecd057eb69db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbb23f4a-f1b5-49ca-a30c-64382d9972f9",
            "compositeImage": {
                "id": "408c57b4-13ea-4056-93b0-4284214a0a99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35d5d87c-2968-43f4-ac7b-ecd057eb69db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9429e9f6-ca44-4b51-b66d-7fa12cefe4a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35d5d87c-2968-43f4-ac7b-ecd057eb69db",
                    "LayerId": "a0807207-12e3-4ddd-82a6-88c7197c5cb2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a0807207-12e3-4ddd-82a6-88c7197c5cb2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbb23f4a-f1b5-49ca-a30c-64382d9972f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}