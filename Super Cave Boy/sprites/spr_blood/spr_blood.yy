{
    "id": "ae395802-8ebb-4baa-ae93-80312c94a0c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "54b22347-90b9-46d7-93bc-3d7cf67c2b00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae395802-8ebb-4baa-ae93-80312c94a0c9",
            "compositeImage": {
                "id": "9fa1a539-36fa-4d22-9e0c-eb9a194edd95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54b22347-90b9-46d7-93bc-3d7cf67c2b00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4be0b7f1-54c1-4311-b285-1dc97b66e9b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54b22347-90b9-46d7-93bc-3d7cf67c2b00",
                    "LayerId": "2064cc46-f295-4932-95b1-909d022c211a"
                }
            ]
        },
        {
            "id": "9a2eb838-f2a6-48e1-851f-567d1d21d3c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae395802-8ebb-4baa-ae93-80312c94a0c9",
            "compositeImage": {
                "id": "5f45d855-72e1-4b78-a0d9-d85b43aaff8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a2eb838-f2a6-48e1-851f-567d1d21d3c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77d70a15-2bd9-4b7d-9d17-f14181e4077d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a2eb838-f2a6-48e1-851f-567d1d21d3c7",
                    "LayerId": "2064cc46-f295-4932-95b1-909d022c211a"
                }
            ]
        },
        {
            "id": "6212d5e1-7d36-4928-b92e-c8a7ff19aae9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae395802-8ebb-4baa-ae93-80312c94a0c9",
            "compositeImage": {
                "id": "2add4dd5-8d54-482c-863a-22edf84946cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6212d5e1-7d36-4928-b92e-c8a7ff19aae9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f486f7d-9039-4b8c-ad6f-7a61aece8092",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6212d5e1-7d36-4928-b92e-c8a7ff19aae9",
                    "LayerId": "2064cc46-f295-4932-95b1-909d022c211a"
                }
            ]
        },
        {
            "id": "f138fbf7-4363-4baa-b560-094ac73ca29b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae395802-8ebb-4baa-ae93-80312c94a0c9",
            "compositeImage": {
                "id": "5b9ef06d-0fe3-440a-9088-45ee96e232f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f138fbf7-4363-4baa-b560-094ac73ca29b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3bb71e9-3b61-44df-b2d6-420e864b680d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f138fbf7-4363-4baa-b560-094ac73ca29b",
                    "LayerId": "2064cc46-f295-4932-95b1-909d022c211a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "2064cc46-f295-4932-95b1-909d022c211a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae395802-8ebb-4baa-ae93-80312c94a0c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}