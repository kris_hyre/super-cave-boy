{
    "id": "725b69fc-7942-41c5-99e7-4d3d58cf75e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_exit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "606296e4-2b8a-4615-a25c-eff215379b23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "725b69fc-7942-41c5-99e7-4d3d58cf75e8",
            "compositeImage": {
                "id": "f42b7689-2762-4d97-adcd-0344b3f79e65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "606296e4-2b8a-4615-a25c-eff215379b23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70495487-5673-479d-b8e2-dbcccbcfb0f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "606296e4-2b8a-4615-a25c-eff215379b23",
                    "LayerId": "73e08695-6b80-47e5-9d8d-c208365fb1ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "73e08695-6b80-47e5-9d8d-c208365fb1ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "725b69fc-7942-41c5-99e7-4d3d58cf75e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}