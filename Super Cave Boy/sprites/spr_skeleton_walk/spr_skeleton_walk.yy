{
    "id": "75a520f9-bfb2-4b01-8460-122edab1e0a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeleton_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a541f435-2a2f-4983-93fd-78a5bfd19967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a520f9-bfb2-4b01-8460-122edab1e0a8",
            "compositeImage": {
                "id": "1be557e8-7316-4e3e-aa8d-5cf0f66b9226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a541f435-2a2f-4983-93fd-78a5bfd19967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e2adb2d-1093-4f0e-9f1f-91d99c2a954b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a541f435-2a2f-4983-93fd-78a5bfd19967",
                    "LayerId": "f2837bea-bf21-4abd-93bc-ceb687a3d4ef"
                }
            ]
        },
        {
            "id": "b073183a-dc1f-42f9-a429-d8182b423023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a520f9-bfb2-4b01-8460-122edab1e0a8",
            "compositeImage": {
                "id": "b0373e75-8041-432d-9813-ed6d69e6845e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b073183a-dc1f-42f9-a429-d8182b423023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62fd270e-d53a-445f-a39d-77a76dea61ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b073183a-dc1f-42f9-a429-d8182b423023",
                    "LayerId": "f2837bea-bf21-4abd-93bc-ceb687a3d4ef"
                }
            ]
        },
        {
            "id": "b556c095-7419-4580-bd67-f4901e50c9b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a520f9-bfb2-4b01-8460-122edab1e0a8",
            "compositeImage": {
                "id": "480ff0ef-d2d2-4c5e-8293-13035d40c30b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b556c095-7419-4580-bd67-f4901e50c9b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cb31fe7-36ff-45b5-9af9-ad7299eb39cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b556c095-7419-4580-bd67-f4901e50c9b5",
                    "LayerId": "f2837bea-bf21-4abd-93bc-ceb687a3d4ef"
                }
            ]
        },
        {
            "id": "61af30c8-b69c-4bbd-8a6a-0fc7034ecabe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a520f9-bfb2-4b01-8460-122edab1e0a8",
            "compositeImage": {
                "id": "7ef5cb41-01a8-435b-a6d3-ec63c90cebfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61af30c8-b69c-4bbd-8a6a-0fc7034ecabe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6274b4a3-4abf-437f-a718-7b1f10e48e9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61af30c8-b69c-4bbd-8a6a-0fc7034ecabe",
                    "LayerId": "f2837bea-bf21-4abd-93bc-ceb687a3d4ef"
                }
            ]
        },
        {
            "id": "a028ec58-19a2-4168-81f1-961917046a6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a520f9-bfb2-4b01-8460-122edab1e0a8",
            "compositeImage": {
                "id": "57e36737-a6f2-425e-9cf9-d20b5e4d5ae0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a028ec58-19a2-4168-81f1-961917046a6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3028b7f-39e9-485e-9eca-c09e219b5719",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a028ec58-19a2-4168-81f1-961917046a6b",
                    "LayerId": "f2837bea-bf21-4abd-93bc-ceb687a3d4ef"
                }
            ]
        },
        {
            "id": "21a67a5a-3deb-4cb3-a7cc-f12daf3680ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a520f9-bfb2-4b01-8460-122edab1e0a8",
            "compositeImage": {
                "id": "6398df8a-7018-47d8-955d-f54186be6892",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21a67a5a-3deb-4cb3-a7cc-f12daf3680ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12f9f1ff-f0b0-43f8-b096-7b8978a6ea38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21a67a5a-3deb-4cb3-a7cc-f12daf3680ab",
                    "LayerId": "f2837bea-bf21-4abd-93bc-ceb687a3d4ef"
                }
            ]
        },
        {
            "id": "4b3bea70-324e-4d66-b1ab-f743b74bbf23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a520f9-bfb2-4b01-8460-122edab1e0a8",
            "compositeImage": {
                "id": "f4e2a6d4-ba9a-439d-8d82-37c875cdde58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b3bea70-324e-4d66-b1ab-f743b74bbf23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ed3d9b3-3d12-432f-8c7d-56506a1097a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b3bea70-324e-4d66-b1ab-f743b74bbf23",
                    "LayerId": "f2837bea-bf21-4abd-93bc-ceb687a3d4ef"
                }
            ]
        },
        {
            "id": "dd17ce0e-6925-4933-a0bf-1ec27ab475d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a520f9-bfb2-4b01-8460-122edab1e0a8",
            "compositeImage": {
                "id": "25d8cbd9-c366-4598-95cd-3f036ac5acd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd17ce0e-6925-4933-a0bf-1ec27ab475d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf653b5c-0c54-4947-9f42-2df178a8bd45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd17ce0e-6925-4933-a0bf-1ec27ab475d5",
                    "LayerId": "f2837bea-bf21-4abd-93bc-ceb687a3d4ef"
                }
            ]
        },
        {
            "id": "826da12b-3135-4fe1-9bc2-01b682e0aa9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a520f9-bfb2-4b01-8460-122edab1e0a8",
            "compositeImage": {
                "id": "82c15172-6e60-4a4c-880b-0c2113a293c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "826da12b-3135-4fe1-9bc2-01b682e0aa9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72e2ca15-acb6-4244-9dce-d12518267c30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "826da12b-3135-4fe1-9bc2-01b682e0aa9c",
                    "LayerId": "f2837bea-bf21-4abd-93bc-ceb687a3d4ef"
                }
            ]
        },
        {
            "id": "e1ea0e4f-a675-4bf7-a5a8-3fb1d976a27d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a520f9-bfb2-4b01-8460-122edab1e0a8",
            "compositeImage": {
                "id": "471e1ef7-c3b0-4f9b-9bae-1a5d00ddf5e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1ea0e4f-a675-4bf7-a5a8-3fb1d976a27d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62ca3cab-cddf-47c1-a515-b530496fd6ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1ea0e4f-a675-4bf7-a5a8-3fb1d976a27d",
                    "LayerId": "f2837bea-bf21-4abd-93bc-ceb687a3d4ef"
                }
            ]
        },
        {
            "id": "eb03dc1c-2e67-45b4-8b6d-d2b2470f6b4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75a520f9-bfb2-4b01-8460-122edab1e0a8",
            "compositeImage": {
                "id": "af6e29f2-2e0e-406c-b5d4-5ee296ec0c68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb03dc1c-2e67-45b4-8b6d-d2b2470f6b4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49e387b5-3976-4907-ab5b-c9c1f9b3cfaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb03dc1c-2e67-45b4-8b6d-d2b2470f6b4a",
                    "LayerId": "f2837bea-bf21-4abd-93bc-ceb687a3d4ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "f2837bea-bf21-4abd-93bc-ceb687a3d4ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75a520f9-bfb2-4b01-8460-122edab1e0a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 19
}