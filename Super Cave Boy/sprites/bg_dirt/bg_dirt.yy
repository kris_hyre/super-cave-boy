{
    "id": "1dd12ea0-3bb5-4abd-9eac-5496e074aff9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_dirt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1251b7f2-6760-4838-833f-9e7e5fb2b6b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dd12ea0-3bb5-4abd-9eac-5496e074aff9",
            "compositeImage": {
                "id": "f179751f-4e7f-4461-8019-bab6daafff7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1251b7f2-6760-4838-833f-9e7e5fb2b6b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d514a06b-8684-4f69-993a-b21346b8a2cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1251b7f2-6760-4838-833f-9e7e5fb2b6b2",
                    "LayerId": "b1d56d9d-c625-4614-ba34-1314177f0863"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b1d56d9d-c625-4614-ba34-1314177f0863",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1dd12ea0-3bb5-4abd-9eac-5496e074aff9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}