///move(collision_object)

var collision_object = argument0;

if (place_meeting(x + hspd, y, collision_object)) {
	while (!place_meeting(x + sign(hspd), y, collision_object)) {
		x += sign(hspd);
	}
	hspd = 0;	
	
	if (state = grapple_state){
		rope_angle = point_direction(grapple_X, grapple_Y, x, y);
		rope_angle_velocity = 0;
	}
}

x += hspd;

if (place_meeting(x, y + vspd, collision_object)) {
	while (!place_meeting(x, y + sign(vspd), collision_object)){
		y += sign(vspd);
	}
	vspd = 0;
	
	if (state = grapple_state){
		rope_angle = point_direction(grapple_X, grapple_Y, x, y);
		rope_angle_velocity = 0;
	}
} 

y += vspd; 