///bat_chase_state()

if (instance_exists(obj_Player)) {
	var dir = point_direction(x, y, obj_Player.x, obj_Player.y);
	hspd = lengthdir_x(spd, dir);
	vspd = lengthdir_y(spd, dir);
	
	// Change to flying sprite
	sprite_index = spr_bat_flying;
	
	// Face direction
	if (hspd != 0) {
		image_xscale = sign(hspd);
	}
	
	// Move	
	move(obj_Solid);
	
}