///bat_idle_state()

image_index = spr_bat_idle;

// Look for the player
if (instance_exists(obj_Player)) {
	var dis = point_distance(x, y, obj_Player.x, obj_Player.y);
	
	if (dis < sight) {
		state = bat_chase_state;
	}
}
