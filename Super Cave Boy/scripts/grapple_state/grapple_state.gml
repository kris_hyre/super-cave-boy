// Grapple State Code
var swing_strength = 0.08;
var climb_speed = 2;

var rope_angle_acceleration = -0.3 * dcos(rope_angle);
rope_angle_acceleration += (right - left) * swing_strength;
rope_length += (down - up_held) * climb_speed;
rope_angle_velocity += rope_angle_acceleration;
rope_length = max(rope_length, 0);
rope_angle += rope_angle_velocity;
rope_angle_velocity *= 0.99; // air friction

rope_X = grapple_X + lengthdir_x(rope_length, rope_angle);
rope_Y = grapple_Y + lengthdir_y(rope_length, rope_angle);

hspd = rope_X - x;
vspd = rope_Y - y;

// Sprite Flip
if (hspd != 0) {
	image_xscale = sign(hspd);
}

move(obj_Solid);

if (grapple_released){
	state = move_state;
	if (up){
		vspd = -jspd;
	}
}